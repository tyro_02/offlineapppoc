import { Component, OnInit, NgZone } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { randomAlphaNumeric } from '../../helper';
import { custSchema } from '../../allSchemas';
import { PouchDBService } from '../../providers/pouch-db.service';

@Component({
  selector: 'app-add-more',
  templateUrl: './add-more.component.html',
  styleUrls: ['./add-more.component.css']
})
export class AddMoreComponent implements OnInit {

  public addMoreForm: FormGroup;
  request: any;

  // tslint:disable-next-line:max-line-length
  constructor(private formBuilder: FormBuilder, public addMoreDialogRef: MatDialogRef<AddMoreComponent>, private database: PouchDBService, private zone: NgZone) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.addMoreForm = this.formBuilder.group({
      title :	['', Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(100),
        Validators.pattern('^[a-zA-Z0-9. ,-]+$')])],

      firstname:  ['', [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(30),
          Validators.pattern('^[a-zA-Z]+$')
      ]],

      lastname: ['', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(30),
        Validators.pattern('^[a-zA-Z]+$')
      ]],

      email:	['', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30),
        Validators.pattern('^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+[.][a-zA-Z]+$')])],

      address :	['', Validators.compose([
          Validators.minLength(10),
          Validators.maxLength(30),
          Validators.pattern('^[a-zA-Z0-9. ,]+$')])]
    });
  }

  cancelAddingCustomer() {
    this.addMoreDialogRef.close();
  }

  addMore() {
    const body = this.addMoreForm.getRawValue();
    this.database.put(new Date().toJSON(), body).then((result: any) => {
        console.log(result);
      }, error => {
        console.error(error);
    });
    this.addMoreDialogRef.close();
  }
}
