import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { AddMoreComponent } from '../add-more/add-more.component';
import { Sort, MatDialog, MatDialogRef, MatDialogConfig, MatTableDataSource, MatTable, MatSort, MatPaginator } from '@angular/material';
import { UpdateDetailsComponent } from '../update-details/update-details.component';
import { custSchema } from '../../allSchemas';
import { PouchDBService } from '../../providers/pouch-db.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  public details: any;
  displayedColumns = ['title', 'firstname', 'lastname', 'email', 'address'];
  dataSource;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  sortedData;

  public addMoreDialogRef: MatDialogRef<AddMoreComponent>;
  public updateDialogRef: MatDialogRef<UpdateDetailsComponent>;

  // tslint:disable-next-line:max-line-length
  constructor(public addMoreDialog: MatDialog, public updateDetailsDialog: MatDialog, private database: PouchDBService, private zone: NgZone) { }

  ngOnInit() {
    this.database.sync(environment.db);
    this.database.getChangeListener().subscribe(data => {
        for (let i = 0; i < data.change.docs.length; i++) {
            this.zone.run(() => {
                this.details.push(data.change.docs[i]);
            });
        }
    });
    this.getLatestDetails();
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  public openAddMoreDialog() {

    const config: MatDialogConfig = {
      disableClose: true,
      width: '350px',
      height: '450px',
      position: {
        top: '',
        bottom: '',
        left: '',
        right: ''
      },
    };

    this.addMoreDialogRef = this.addMoreDialog.open(AddMoreComponent, config);
    this.addMoreDialogRef.afterClosed().subscribe((result) => {
      this.getLatestDetails();
    });
  }

  private  getLatestDetails() {
    this.details = [];
    this.database.fetch().then(result => {
      for (let i = 0; i < result.rows.length; i++) {
        this.details.push(result.rows[i].doc);
      }
      this.dataSource = new MatTableDataSource<Details>(this.details);
      this.dataSource.paginator = this.paginator;
    }, error => {
        console.error(error);
    });
  }

  sortData(sort: Sort) {
    const data = this.details.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'title': return this.compare(a.title, b.title, isAsc);
        case 'firstname': return this.compare(+a.firstname, +b.firstname, isAsc);
        case 'lastname': return this.compare(+a.lastname, +b.lastname, isAsc);
        case 'email': return this.compare(+a.email, +b.email, isAsc);
        default: return 0;
      }
    });
  }

  compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  public handleRowClick(row) {
      const config: MatDialogConfig = {
        disableClose: true,
        width: '350px',
        height: '450px',
        position: {
          top: '',
          bottom: '',
          left: '',
          right: ''
        },
        data: row
      };
      this.updateDialogRef = this.updateDetailsDialog.open(UpdateDetailsComponent, config);
      this.updateDialogRef.afterClosed().subscribe((result) => {
        this.getLatestDetails();
      });
    }
}

export interface Details {
  title: string;
  firstname: string;
  lastname: string;
  email: string;
  address: string;
  _id: string;
  _rev: string;
}
