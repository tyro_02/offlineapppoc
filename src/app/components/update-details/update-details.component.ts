import { Component, OnInit, Inject, OnChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { PouchDBService } from '../../providers/pouch-db.service';

@Component({
  selector: 'app-update-details',
  templateUrl: './update-details.component.html',
  styleUrls: ['./update-details.component.css']
})
export class UpdateDetailsComponent implements OnInit, OnChanges {

  public updateForm: FormGroup;
  private formUpdated = false;

  // tslint:disable-next-line:max-line-length
  constructor(@Inject(MAT_DIALOG_DATA) public singleDetails: any,  private database: PouchDBService, private formBuilder: FormBuilder, public updateDialogRef: MatDialogRef<UpdateDetailsComponent>) { }

  ngOnInit() {
    this.createForm();

    this.ngOnChanges();
  }

  ngOnChanges() {
    this.updateForm.valueChanges
    .subscribe(changes => {
      console.log('Form updated');
      this.formUpdated = true;
    });
  }

  createForm() {
    this.updateForm = this.formBuilder.group({
      title :	[this.singleDetails.title, Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(100),
        Validators.pattern('^[a-zA-Z0-9. ,-]+$')])],

      firstname:  [this.singleDetails.firstname, [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(30),
          Validators.pattern('^[a-zA-Z]+$')
      ]],

      lastname: [this.singleDetails.lastname, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(30),
        Validators.pattern('^[a-zA-Z]+$')
      ]],

      email:	[this.singleDetails.email, Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30),
        Validators.pattern('^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+[.][a-zA-Z]+$')])],

      address :	[this.singleDetails.address, Validators.compose([
          Validators.minLength(10),
          Validators.maxLength(30),
          Validators.pattern('^[a-zA-Z0-9. ,]+$')])]
    });
  }

  cancelUpdatingCustomer() {
    this.updateDialogRef.close();
  }

  updateDetails() {
    console.log(this.formUpdated);

    if (!this.formUpdated) {
      alert('No field has been changed to update details!');
    } else {
    const updatedRow = this.updateForm.getRawValue();
      this.database.put(this.singleDetails._id, updatedRow).then((result: any) => {
          console.log(result);
      }, error => {
          console.error(error);
      });
    }
    this.updateDialogRef.close();
  }

}
