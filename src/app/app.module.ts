import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app.routing.module';
import { AppComponent } from './app.component';
import { AddMoreComponent } from './components/add-more/add-more.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { UpdateDetailsComponent } from './components/update-details/update-details.component';
import { PouchDBService } from './providers/pouch-db.service';

// tslint:disable-next-line:max-line-length
import { MatInputModule, MatCardModule, MatDialogModule, MatDialog, MatFormFieldModule, MatPaginatorModule, MatTableModule, MatSortModule } from '@angular/material';


@NgModule({
  declarations: [
    AppComponent,
    AddMoreComponent,
    DashboardComponent,
    UpdateDetailsComponent,
  ],
  imports: [
    BrowserModule,
    MatDialogModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatPaginatorModule,
    MatTableModule,
    MatSortModule
  ],
  entryComponents: [AddMoreComponent, UpdateDetailsComponent],
  providers: [PouchDBService],
  bootstrap: [AppComponent]
})
export class AppModule { }
