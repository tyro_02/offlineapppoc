import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
// import { SigninComponent } from './signin/signin.component';
import { DashboardComponent } from '../app/components/dashboard/dashboard.component';
// import { AuthGuard } from './auth.guard';

const routes: Routes = [
  // { path: '', component: SigninComponent },
  // { path: 'login', component: SigninComponent },
  // { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },

   // otherwise redirect to home
   { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [ CommonModule, RouterModule.forRoot(routes, { useHash: true })],
  exports: [ RouterModule ],
  declarations: []
})

export class AppRoutingModule { }
