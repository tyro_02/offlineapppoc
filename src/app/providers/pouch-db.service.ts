import { Injectable, EventEmitter } from '@angular/core';
import PouchDB from 'pouchdb';

@Injectable({
  providedIn: 'root'
})
export class PouchDBService {

  private isInstantiated: boolean;
  private database: any;
  private listener: EventEmitter<any> = new EventEmitter();

  public constructor() {
      if (!this.isInstantiated) {
          this.database = new PouchDB('testdb');
          this.isInstantiated = true;
      }
  }

  public fetch() {
    return this.database.allDocs({include_docs: true});
  }

  public get(id: string) {
    return this.database.get(id);
  }

  public put(id: string, document: any) {
    console.log('in service put');
    document._id = id;
    return this.get(id).then(result => {
      console.log('in service put trying to get doc with id if exists');
        document._rev = result._rev;
        return this.database.put(document);
    }, error => {
        if (error.status === 404) {
          console.log('in service put; couldnt find existing id so here it is');
            return this.database.put(document);
        } else {
            return new Promise((resolve, reject) => {
              console.log('in service put rejected');
                reject(error);
            });
        }
    });
  }

  public sync(remote: string) {
    const remoteDatabase = new PouchDB(remote);
    this.database.sync(remoteDatabase, {
        live: true
    }).on('change', change => {
        this.listener.emit(change);
    });
  }

  public getChangeListener() {
    return this.listener;
   }
}
